(function ($) {
    "use strict";

    jQuery(document).ready(function ($) {


        $(".embed-responsive iframe").addClass("embed-responsive-item");
        $(".carousel-inner .item:first-child").addClass("active");

        $('[data-toggle="tooltip"]').tooltip();






        $(function () {
            $('.nav-toggle-btn').click(function () {
                $('body').toggleClass('active-nav');
                return false;
            });

        });


        //Smooth menu    
        $('li.smooth-menu a').click(function () {
            var headerH = "70";
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - headerH + 'px'
            }, 1208, 'easeInOutExpo');

        });






        $(".slider").owlCarousel({
            items: 1,
            loop: true,
            smartSpeed: 1000,
            nav: true,
            autoplay: true,
            mouseDrag: false,
            touchDrag: false,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
        });

        $(".slider").on('translate.owl.carousel', function () {
            $('.sl-cntn h1').removeClass('fadeInUp animated').hide();
            $('.sl-cntn p').removeClass('fadeInUp animated').hide();
            $('.sl-cntn h2').removeClass('fadeInUp animated').hide();
            $('.slide-btn a').removeClass('fadeInUp animated').hide();
        });
        $(".slider").on('translated.owl.carousel', function () {
            $('.owl-item.active .sl-cntn h1').addClass('fadeInUp animated').show();
            $('.owl-item.active .sl-cntn p').addClass('fadeInUp animated').show();
            $('.owl-item.active .sl-cntn h2').addClass('fadeInUp animated').show();
            $('.owl-item.active .slide-btn a').addClass('fadeInUp animated').show();
        });



        $(function () {
            $(".meter span").each(function () {
                $(this)
                    .data("origWidth", $(this).width())
                    .width(0)
                    .animate({
                        width: $(this).data("origWidth")
                    }, 1200);
            });
        });


        /* Protfolio area Start */


        $('.protfolio-iso li').click(function () {
            $('.protfolio-iso li').removeClass('active');
            $(this).addClass('active');

            var selector = jQuery(this).attr('data-filter');
            $('.protfolio').isotope({
                filter: selector,
                itemSelector: '.col-md-3',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false,
                }
            });
            return false;

        });



        $('.protfolio').isotope({
            itemSelector: '.col-md-3'
        });


        /* Protfolio area End */


        $(".header-area").sticky({
            topSpacing: 0
        });



    });


    jQuery(window).load(function () {


    });


}(jQuery));