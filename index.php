<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Imran Hoshain (Spy)</title>
    <!-- font awesome -->
    <!--<link href="https://fonts.googleapis.com/css?family=Lato:100,400,700|Montserrat:400,700" rel="stylesheet">-->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/lightbox.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link href="style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon">


    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--header area + Slidaer Area Start-->
    <div id="home" class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo">
                        <a href="#"><span class="logo-text">Imran hoshain</span></a>
                    </div>
                </div>
                <div class="col-sm-9">
                    <nav>
                        <a href="#" class="nav-toggle-btn"><i class="fa fa-bars"></i></a>
                        <div class="mainmenu">
                            <ul>
                                <li class="smooth-menu"><a href="#home">Home</a></li>
                                <li class="smooth-menu"><a href="#about">About </a></li>
                                <li class="smooth-menu"><a href="#service">Service </a></li>
                                <li class="smooth-menu"><a href="#portfolio">Portfolio </a></li>
                                <li class="smooth-menu"><a href="#testmonial">Testmonial </a></li>
                                <li class="smooth-menu"><a href="#contact">Contact </a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div clss="slider-area">
        <div class="slider">
            <div class="item" style="background:url(img/imran.jpg) no-repeat scroll center center / cover;">
                <div class="slider-table">
                    <div class="slider-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sl-cntn">
                                        <h1>Imran Hoshain</h1>
                                        <p>Craetive-Professional-Passionate</p>
                                        <h2>Web Designer & Devoloper</h2>
                                        <div class="slide-btn">
                                            <a href="#">Give me a chance to show who am i</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- item -->
            <div class="item" style="background:url(img/imran1.jpg) no-repeat scroll center center / cover;">
                <div class="slider-table">
                    <div class="slider-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sl-cntn">
                                        <h1>Imran Hoshain</h1>
                                        <p>Craetive-Professional-Passionate</p>
                                        <h2>Web Designer & Devoloper</h2>
                                        <div class="slide-btn">
                                            <a href="#">Give me a chance to show who am i</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- item -->
            <!-- item -->
            <div class="item" style="background:url(img/imran2.jpg) no-repeat scroll center center / cover;">
                <div class="slider-table">
                    <div class="slider-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sl-cntn">
                                        <h1>Imran Hoshain</h1>
                                        <p>Craetive-Professional-Passionate</p>
                                        <h2>Web Designer & Devoloper</h2>
                                        <div class="slide-btn">
                                            <a href="#">Give me a chance to show who am i</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- item -->
        </div>
    </div>

    <!--header area + Slidaer Area End-->

    <!--About area start-->
    <div id="about" class="about-area">
        <div class="container">
            <div class="about-txt sp30">
                <h1>ABOUT ME</h1>
                <h5>Always be yourself, express yourself, have faith in yourself, do not go out and look for a successful personality and duplicate it.</h5>
                <div class="row">
                    <div class="col-md-3">
                        <div class="about-info">
                            <img src="img/inf.jpg" alt=""><br><br>
                            <a href="#">Protfolio</a>
                            <a href="#">Contact</a>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="about-info expt">
                            <h3>My Expartise</h3>
                            <div class="progressbar">
                                <div class="html">
                                    <div class="meter">
                                        <span style="width: 92%"><p>HTML5 80%</p></span>
                                    </div>
                                </div>
                                <br>
                                <div class="css">
                                    <div class="meter">
                                        <span style="width: 80%"><p>CSS3 75%</p></span>
                                    </div>
                                </div>
                                <br>
                                <div class="psdtohtml bar">
                                    <div class="meter">
                                        <span style="width: 95%"><p>PSD to HTML 95%</p></span>
                                    </div>
                                </div>
                                <br>
                                <div class="php">
                                    <div class="meter">
                                        <span style="width: 50%"><p>PHP 50%</p></span>
                                    </div>
                                </div>
                                <br>
                                <div class="jquary">
                                    <div class="meter">
                                        <span style="width: 30%"><p>JQUARY 30%</p></span>
                                    </div>
                                </div>
                                <br>
                                <div class="wordpress">
                                    <div class="meter">
                                        <span style="width: 42%"><p>WORDPRESS 42%</p></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="expt">
                            <h3>My Resume</h3>
                            <div class="row">
                                <!-- Nav tabs -->
                                <ul class="about-menu">
                                    <li class="active"><a href="#my" data-toggle="tab">Why Me</a></li>
                                    <li><a href="#edu" data-toggle="tab">Education</a></li>
                                    <li><a href="#emp" data-toggle="tab">Employment</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="catagory tab-pane active" id="my">
                                        <ol>
                                            <li>Honest</li>
                                            <li>Affordable</li>
                                            <li>Self Modivative</li>
                                            <li>Quality Of Work</li>
                                            <li>Friendly Communication</li>
                                            <li>Responce in all Time</li>
                                        </ol>
                                    </div>
                                    <div class="catagory tab-pane" id="edu">
                                        <ol>
                                            <h6>Tangail Polytechnic Institute</h6>
                                            <p>Diploma In Computer / 2016</p>

                                            <h6>Govt. Asheq Mahmud College</h6>
                                            <p>H.S.C / 2012</p>

                                            <h6>Jamalpur Zilla School</h6>
                                            <p>S.S.C / 2010</p>
                                        </ol>
                                    </div>
                                    <div class="catagory tab-pane" id="emp">
                                        <ol>
                                            <strong>Upwork  Web Designer & Developer</strong>
                                            <p>July 2016-Continue</p>

                                            <strong>RRF Dhaka / Web Designer & Developer</strong>
                                            <p>July 2016-Jan 2017</p>
                                        </ol>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="service" class="about-btm sp30">
            <h1>What I Do</h1>
            <h5>With certainty, you have won before you have begun.</h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="about-exprt">
                        <i class="fa fa-desktop" aria-hidden="true"></i><br>
                        <a href="">Web Design</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="about-exprt">
                        <i class="fa fa-code" aria-hidden="true"></i><br>
                        <a href="">Web Development</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="about-exprt">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i><br>
                        <a href="">Web Redesign</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="about-exprt">
                        <i class="fa fa- fa-tablet" aria-hidden="true"></i><br>
                        <a href="">Responsive Web Design</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="about-exprt">
                        <i class="fa fa- fa-keyboard-o" aria-hidden="true"></i><br>
                        <a href="">Website Maintenance</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="about-exprt">
                        <i class="fa fa- fa-chain" aria-hidden="true"></i><br>
                        <a href="">WordPress</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--About area End-->

    <!--Protfolio area Start-->

    <div id="portfolio" class="protfolio-area">
        <div class="protfolio-txt sp30">
            <h1>Protfolio</h1>
            <h5>Se my work</h5>
        </div>
        <div class="protfolio-menu">
            <div class="container">
                <div class="protfolio-iso">
                    <li data-filter="*" class="active">All</li>
                    <li data-filter=".single">Single Page Site</li>
                    <li data-filter=".ecommers">Ecommers Webpage</li>
                    <li data-filter=".Business">Business Webpage</li>
                    <li data-filter=".education">Educational Site</li>
                    <li data-filter=".creative">Creative Site</li>
                </div>
            </div>
        </div>
        <div class="row protfolio">
            <div class="col-md-3 single">
                <div class="pr-img">
                    <img src="img/pr1.png" alt="">
                </div>
            </div>
            <div class="col-md-3 single">
                <div class="pr-img">
                    <img src="img/pr2.png" alt="">
                </div>
            </div>
            <div class="col-md-3 single">
                <div class="pr-img">
                    <img src="img/pr3.png" alt="">
                </div>
            </div>
            <div class="col-md-3 ecommers">
                <div class="pr-img">
                    <img src="img/pr4.png" alt="">
                </div>
            </div>
            <div class="col-md-3 ecommers">
                <div class="pr-img">
                    <img src="img/pr7.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 creative">
                <div class="pr-img">
                    <img src="img/pr7.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 Business">
                <div class="pr-img">
                    <img src="img/pr7.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 Business">
                <div class="pr-img">
                    <img src="img/pr7.jpg" alt="">
                </div>
            </div>
            <div class="load-more">
                <div class="col-md-3 ecommers">
                    <div class="pr-img">
                        <img src="img/tangailsari.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-3 ecommers">
                    <div class="pr-img">
                        <img src="img/pr7.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Protfolio area End-->


    <!--Testimonial area Start-->

    <div id="testmonial" class="testimonial-area">
        <div class="container">
            <div class="testimonial-txt sp30">
                <h1>Testimonial</h1>
                <h5>What clients say about me</h5>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="test-cntn">
                        <q>“Fantastic freelance. Great to work with and already planning to hire again. 10/10 Well done”</q><br><br>
                        <div class="test-prof">
                            <i class="fa fa-user"></i>
                            <span class="person-name">WEB DESIGN</span> ,
                            <span class="person-meta">Upwork</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="test-cntn">
                        <q>“Excellent quality work and communication, well recommended and very skilled developer and fast also.”</q><br><br>
                        <div class="test-prof">
                            <i class="fa fa-user"></i>
                            <span class="person-name">WEB Development</span> ,
                            <span class="person-meta">Upwork</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="test-cntn">
                        <q>“He did everything I asked without complaint. Great work over all.”</q><br><br>
                        <div class="test-prof">
                            <i class="fa fa-user"></i>
                            <span class="person-name">SEO</span> ,
                            <span class="person-meta">Freelancer</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="test-cntn">
                        <q>“Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas faucibus mollis interdum.”</q><br><br>
                        <div class="test-prof">
                            <i class="fa fa-user"></i>
                            <span class="person-name">WEB DESIGN</span> ,
                            <span class="person-meta">Freelancer</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="test-cntn">
                        <q>“Very good work. Really happy with all things delivered. Thank you so much for your cooperation.”</q><br><br>
                        <div class="test-prof">
                            <i class="fa fa-user"></i>
                            <span class="person-name">WEB DESIGN</span> ,
                            <span class="person-meta">Upwork</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="test-cntn">
                        <q>“Excellent Designer, did a Awesome job.Next time I hire you.Thank you”</q><br><br>
                        <div class="test-prof">
                            <i class="fa fa-user"></i>
                            <span class="person-name">WEB DESIGN</span> ,
                            <span class="person-meta">PeoplePerHour</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Testimonial area End-->



    <!--Contact area Start-->

    <div id="contact" class="contact-area">
        <div class="container">
            <div class="contact-txt sp30">
                <h1>Contact Me</h1>
                <h5>Let’s Get In Touch</h5>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="contact">
                        <h3>Contact Info:</h3>
                    </div>
                    <div class="contact-detail">
                        <div class="skype">
                            <i class="fa fa-skype"></i>
                            <span><a href="skype:i4u.imran">Imran Hoshain</a></span>
                        </div>
                        <div class="phone">
                            <i class="fa fa-phone"></i>
                            <span><a href="tel:+8801723333608">+8801723333608</a></span>
                        </div>
                        <div class="email">
                            <i class="fa fa-envelope"></i>
                            <span><a href="mailto:princ.imran@gmail.com">princ.imran@gmail.com</a></span>
                        </div>
                    </div>
                    <div class="social-icon">
                        <h2>Find me on social</h2><br>
                        <a href="mailto:princ.imran@gmail.com">
                            <i class="fa fa-envelope"></i>
                        </a>
                        <a href="https://www.facebook.com/spyhacker2u">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="https://www.twitter.com/i4u_imran">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="http://plus.google.com/u/0/+">
                            <i class="fa fa-google-plus"></i>
                        </a>
                        <a href="http://www.linkedin.com/in/">
                            <i class="fa fa-linkedin"></i>
                        </a>
                        <a href="http://www.youtube.com/">
                            <i class="fa fa-youtube-play"></i>
                        </a>
                        <a href="https://www.vimeo.com/">
                            <i class="fa fa-vimeo-square"></i>
                        </a>
                        <a href="http://www.instagram.com/">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a href="http://www.pinterest.com/">
                            <i class="fa fa-pinterest"></i>
                        </a>
                        <a href="http://www.soundcloud.com/">
                            <i class="fa fa-soundcloud"></i>
                        </a>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="massage-me">
                        <h3>Let’s discuss about your project!!</h3>
                    </div>

                    <div class="massage-me">
                        <p><span><input name="your-name" value="" size="40" placeholder="Name" type="text"></span><br>
                            <span><input name="your-email" value="" size="40" placeholder="Email" type="email"></span><br>
                            <span><input name="your-subject" value="" size="40"  placeholder="Subject" type="text"></span><br>
                            <span><input name="website" value="" size="40" placeholder="Your current Website" type="text"></span><br>
                            <span><textarea name="your-message" cols="80" rows="5" placeholder="Message"></textarea></span><br>
                            <input value="Send" type="submit">
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Contact area End-->



    <!--Footer area Start-->

    <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer-left">
                        <h3>Copy Write &copy 2017 All right reserved</h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footer-right">
                        <h3>Powered By Imran Hoshain</h3>
                    </div>
                </div>
            </div>
        </div>

        <!--Footer area End-->



        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <!--<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>-->
        <script src="js/lightbox.min.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/main.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>



        <script type="text/javascript" src="js/scrolltotop.js"></script>

</body>

</html>